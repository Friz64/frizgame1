<img src="logo.png" height=200>

# frizgame1

Game project by Friz64.

[Download the latest build here](https://gitlab.com/Friz64/frizgame1/-/jobs/artifacts/master/download?job=build)

## Development

Use the dev script like this
```sh
./dev.sh #log #argument #argument #argument ...
```

### Example: Debug log level + Vulkan validation

```sh
./dev.sh debug -v
```

## Goals

### Short term

- Text rendering

### Medium term

- Physics
- Shadow mapping

### Longer term

- Marching cubes on GPU

## Licensing

This project is licensed under the [zlib License](LICENSE).

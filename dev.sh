#!/bin/sh
set -e # exit on errors
cd $(dirname $0)

./shadercomp.sh

export RUST_LOG=frizgame1=$1
export VK_LAYER_ENABLES=VK_VALIDATION_FEATURE_ENABLE_DEBUG_PRINTF_EXT
cargo run --release -- "${@:2}"

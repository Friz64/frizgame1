#!/bin/sh

# Shader compile script
# - Works with vertex, fragment and compute shaders: *.{vfc}s.glsl
# - Only recompiles shaders which have changed since the script was last run

set -e

script=$0
script_dir=$(dirname "$script")
vertex_shaders=$(find $script_dir -type f -name "*.vs.glsl")
fragment_shaders=$(find $script_dir -type f -name "*.fs.glsl")
compute_shaders=$(find $script_dir -type f -name "*.cs.glsl")

total_count=0
count=0
function compile {
    stage="$1"
    input="$2"

    total_count=$((total_count + 1))
    if [ "$input" -nt "$script" ]; then
        output="$(dirname $input)/$(basename $input .glsl).spv"
        glslc -fshader-stage=$stage -g -O -o $output $input
        count=$((count + 1))
    fi
}

for vertex_shader in $vertex_shaders; do
    compile vertex $vertex_shader
done

for fragment_shader in $fragment_shaders; do
    compile fragment $fragment_shader
done

for compute_shader in $compute_shaders; do
    compile compute $compute_shader
done

echo "Successfully recompiled $count out of $total_count shader(s)"
touch $script

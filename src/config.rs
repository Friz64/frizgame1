use color_eyre::eyre::{Result, WrapErr};
use fs::File;
use ron::ser::PrettyConfig;
use serde::{Deserialize, Serialize};
use std::{fs, path::Path};

const CONFIG_NAME: &str = "frizgame1.ron";

#[derive(Debug, Serialize, Deserialize)]
pub enum Framerate {
    Unlimited,
    Limited(f32),
    Sync,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct Config {
    pub framerate: Framerate,
    pub fullscreen: bool,
    pub fov: f32,
    pub mouse_sensitivity: f64,
}

impl Default for Config {
    fn default() -> Config {
        Config {
            framerate: Framerate::Limited(60.0),
            fullscreen: false,
            fov: 45.0,
            mouse_sensitivity: 1.0,
        }
    }
}

impl Config {
    pub fn load() -> Result<Config> {
        let path = Path::new(CONFIG_NAME);
        let mut config = if path.exists() {
            let file = File::open(path).wrap_err("Failed to open config file")?;
            ron::de::from_reader(file).wrap_err("Failed to parse config")?
        } else {
            log::warn!("Config not found, writing default config");
            let default = Config::default();
            default.save().wrap_err("Failed to save default config")?;
            default
        };

        config.fov = config.fov.max(30.0).min(140.0);
        Ok(config)
    }

    pub fn save(&self) -> Result<()> {
        let pretty = ron::ser::to_string_pretty(&self, PrettyConfig::new())
            .expect("Failed to serialize config");
        fs::write(CONFIG_NAME, pretty).wrap_err("Failed to write config file")?;
        Ok(())
    }
}

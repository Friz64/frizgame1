mod passes;
mod physical;
mod swapchain;
mod vk_context;

use crate::{window::Window, Game};
use color_eyre::eyre::{Result, WrapErr};
use erupt::vk;
use glam::Mat4;
use passes::Passes;
use std::{convert::TryInto, sync::Arc};
use swapchain::Swapchain;
use vk_context::VkContext;

const FRAMES_IN_FLIGHT: usize = 2;

pub struct Vulkan {
    ctx: Arc<VkContext>,
    swapchain: Swapchain,
    image_available_semaphores: PerInFlight<vk::Semaphore>,
    render_finished_semaphores: PerInFlight<vk::Semaphore>,
    in_flight_fences: PerInFlight<vk::Fence>,
    passes: Passes,
    in_flight_idx: usize,
}

impl Vulkan {
    pub fn new(window: &Window, game: &Game) -> Result<Self> {
        let ctx = Arc::new(
            VkContext::new(window, game.opt.validation)
                .wrap_err("Failed to obtain Vulkan context")?,
        );
        let swapchain = Swapchain::new(&ctx, &game.config.framerate, None)
            .wrap_err("Failed to create swapchain")?;

        let image_available_semaphores = (0..FRAMES_IN_FLIGHT)
            .map(|_| ctx.create_semaphore("Image Available Semaphore", false))
            .collect::<Result<Vec<_>, _>>()
            .wrap_err("Failed to create a semaphore")?
            .into();

        let render_finished_semaphores = (0..FRAMES_IN_FLIGHT)
            .map(|_| ctx.create_semaphore("Render Finished Semaphore", false))
            .collect::<Result<Vec<_>, _>>()
            .wrap_err("Failed to create a semaphore")?
            .into();

        let in_flight_fences = (0..FRAMES_IN_FLIGHT)
            .map(|_| ctx.create_fence("In Flight Fence", true))
            .collect::<Result<Vec<_>, _>>()
            .wrap_err("Failed to create a fence")?
            .into();

        let passes = Passes::new(&ctx, &swapchain).wrap_err("Failed to create passes")?;

        Ok(Vulkan {
            ctx,
            swapchain,
            image_available_semaphores,
            render_finished_semaphores,
            in_flight_fences,
            passes,
            in_flight_idx: 0,
        })
    }

    pub fn frame(&mut self, game: &Game) -> Result<()> {
        let image_available_semaphore = self.image_available_semaphores.0[self.in_flight_idx];
        let render_finished_semaphore = self.render_finished_semaphores.0[self.in_flight_idx];
        let in_flight_fence = self.in_flight_fences.0[self.in_flight_idx];

        unsafe {
            self.ctx
                .device()
                .wait_for_fences(&[in_flight_fence], true, u64::MAX)
                .result()
                .wrap_err("Failed to wait for in flight fence")?;

            self.ctx
                .device()
                .reset_fences(&[in_flight_fence])
                .result()
                .wrap_err("Failed to reset in flight fence")?;
        }

        let (image, swapchain_recreated) = self
            .swapchain
            .next_image(&self.ctx, &game.config.framerate, image_available_semaphore)
            .wrap_err("Failed to retrieve next swapchain image")?;

        let objects = {
            let mut objects = vec![];

            //objects.push(("grid", Mat4::IDENTITY));

            for physics_object in game.logic.physics.objects() {
                objects.push((
                    physics_object.collider_type().graphical_model_name(),
                    Mat4::from_cols_array(
                        physics_object
                            .cached_frame_transform()
                            .to_homogeneous()
                            .prepend_nonuniform_scaling(
                                &physics_object.collider_type().graphical_model_scale(),
                            )
                            .as_slice()
                            .try_into()
                            .unwrap(),
                    ),
                ));
            }

            objects
        };

        let command_buffer = self
            .passes
            .run(
                game,
                &self.ctx,
                self.in_flight_idx,
                &self.swapchain,
                &image,
                swapchain_recreated,
                &objects,
            )
            .wrap_err("Failed to run passes")?;

        let wait_semaphores = &[image_available_semaphore];
        let wait_stages = &[vk::PipelineStageFlags::COLOR_ATTACHMENT_OUTPUT];
        let command_buffers = &[command_buffer];
        let signal_semaphores = &[render_finished_semaphore];
        let submit_info = vk::SubmitInfoBuilder::new()
            .wait_semaphores(wait_semaphores)
            .wait_dst_stage_mask(wait_stages)
            .command_buffers(command_buffers)
            .signal_semaphores(signal_semaphores);

        unsafe {
            self.ctx
                .device()
                .queue_submit(self.ctx.queue(), &[submit_info], Some(in_flight_fence))
                .result()
                .wrap_err("Failed to submit graphics+present command buffer")?;
        }

        self.swapchain
            .present(&self.ctx, &image, &[render_finished_semaphore])
            .wrap_err("Failed to present swapchain image")?;

        self.in_flight_idx = (self.in_flight_idx + 1) % FRAMES_IN_FLIGHT;
        Ok(())
    }

    pub fn destroy(&mut self) -> Result<()> {
        unsafe {
            self.ctx
                .device()
                .device_wait_idle()
                .result()
                .wrap_err("Failed to wait for device idle")?;

            self.passes.destroy(&self.ctx);

            for &semaphore in self
                .image_available_semaphores
                .0
                .iter()
                .chain(self.render_finished_semaphores.0.iter())
            {
                self.ctx.device().destroy_semaphore(Some(semaphore), None);
            }

            for &fence in &self.in_flight_fences.0 {
                self.ctx.device().destroy_fence(Some(fence), None);
            }
        }

        self.swapchain.destroy(&self.ctx);
        self.ctx
            .destroy()
            .wrap_err("Failed to destroy Vulkan context")?;
        Ok(())
    }
}

#[derive(Copy, Clone)]
pub struct PerInFlight<T>([T; FRAMES_IN_FLIGHT]);

impl<T: Copy> From<Vec<T>> for PerInFlight<T> {
    fn from(vec: Vec<T>) -> Self {
        vec.as_slice().into()
    }
}

impl<T: Copy> From<&[T]> for PerInFlight<T> {
    fn from(slice: &[T]) -> Self {
        PerInFlight(slice.try_into().expect("Vec does not fit"))
    }
}

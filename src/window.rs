use crate::{include_res, Game, Result};
use color_eyre::eyre::{eyre, WrapErr};
use glfw::{ClientApiHint, CursorMode, Glfw, WindowEvent, WindowHint, WindowMode};
use image::{ImageFormat, RgbaImage};
use std::{cell::RefCell, rc::Rc, sync::mpsc::Receiver};

fn png_to_rgba8(bytes: &[u8]) -> RgbaImage {
    let image = image::load_from_memory_with_format(bytes, ImageFormat::Png).unwrap();
    image.to_rgba8()
}

pub struct Window {
    glfw: Glfw,
    window: Rc<RefCell<glfw::Window>>,
    receiver: Receiver<(f64, WindowEvent)>,
    is_fullscreen: bool,
    last_pos: Rc<RefCell<(i32, i32)>>,
    last_size: Rc<RefCell<(i32, i32)>>,
    is_grabbed: bool,
    last_cursor_x: f64,
    last_cursor_y: f64,
    cursor_x: f64,
    cursor_y: f64,
    ignore_cursor_delta: bool,
}

impl Window {
    pub fn new(game: &Game) -> Result<Window> {
        let mut glfw = glfw::init(glfw::LOG_ERRORS).wrap_err("Failed to init GLFW")?;
        glfw.window_hint(WindowHint::ClientApi(ClientApiHint::NoApi));
        glfw.window_hint(WindowHint::AutoIconify(false));

        let (mut window, receiver) = glfw
            .create_window(1280, 720, "frizgame1", WindowMode::Windowed)
            .ok_or_else(|| eyre!("Failed to create GLFW window"))?;
        window.set_size_limits(Some(1), Some(1), Some(16000), Some(16000));
        window.set_close_polling(true);
        window.set_key_polling(true);
        window.set_cursor_pos_polling(true);
        window.set_mouse_button_polling(true);
        window.set_scroll_polling(true);

        window.set_icon(vec![
            png_to_rgba8(include_res!("icon-32x32.png")),
            png_to_rgba8(include_res!("icon-64x64.png")),
            png_to_rgba8(include_res!("icon-128x128.png")),
        ]);

        let last_pos = window.get_pos();
        let last_size = window.get_size();
        let mut window = Window {
            glfw,
            window: Rc::new(RefCell::new(window)),
            receiver,
            is_fullscreen: false,
            last_pos: Rc::new(RefCell::new(last_pos)),
            last_size: Rc::new(RefCell::new(last_size)),
            is_grabbed: false,
            last_cursor_x: 0.0,
            last_cursor_y: 0.0,
            cursor_x: 0.0,
            cursor_y: 0.0,
            ignore_cursor_delta: true,
        };

        window.set_fullscreen(game.config.fullscreen);
        Ok(window)
    }

    /// Returns cursor delta
    pub fn fetch_events(&mut self, events: &mut Vec<WindowEvent>) -> (f64, f64) {
        self.glfw.poll_events();
        self.last_cursor_x = self.cursor_x;
        self.last_cursor_y = self.cursor_y;
        events.clear();
        while let Ok((_timer, event)) = self.receiver.try_recv() {
            if let WindowEvent::CursorPos(x, y) = &event {
                self.cursor_x = *x;
                self.cursor_y = *y;
            }

            events.push(event);
        }

        let dx = self.cursor_x - self.last_cursor_x;
        let dy = self.cursor_y - self.last_cursor_y;
        if (dx != 0.0 || dy != 0.0) && self.ignore_cursor_delta {
            self.ignore_cursor_delta = false;
            (0.0, 0.0)
        } else {
            (dx, dy)
        }
    }

    pub fn set_fullscreen(&mut self, fullscreen: bool) {
        if self.is_fullscreen == fullscreen {
            return;
        }

        if fullscreen {
            let window = self.window.clone();
            let last_pos = self.last_pos.clone();
            let last_size = self.last_size.clone();

            let (window_left, window_top) = self.window.borrow().get_pos();
            let (window_width, window_height) = self.window.borrow().get_size();
            let window_right = window_left + window_width;
            let window_bottom = window_top + window_height;
            self.glfw.with_connected_monitors(|_, monitors| {
                // Choose the monitor which contains the highest amount of visible window area
                let (monitor, video_mode) = monitors
                    .iter()
                    .rev()
                    .map(|monitor| {
                        let video_mode = monitor.get_video_mode().unwrap();
                        (monitor, video_mode)
                    })
                    .max_by_key(|(monitor, video_mode)| {
                        let (monitor_left, monitor_top) = monitor.get_pos();
                        let monitor_right = monitor_left + video_mode.width as i32;
                        let monitor_bottom = monitor_top + video_mode.height as i32;

                        let visible_left = window_left.max(monitor_left).min(monitor_right);
                        let visible_top = window_top.max(monitor_top).min(monitor_bottom);
                        let visible_right = window_right.min(monitor_right).max(monitor_left);
                        let visible_bottom = window_bottom.min(monitor_bottom).max(monitor_top);
                        (visible_right - visible_left) * (visible_bottom - visible_top)
                    })
                    .unwrap();

                let monitor_name = monitor.get_name().unwrap_or_else(|| "(unnamed)".into());

                *last_pos.borrow_mut() = window.borrow().get_pos();
                *last_size.borrow_mut() = window.borrow().get_size();
                window.borrow_mut().set_monitor(
                    WindowMode::FullScreen(monitor),
                    0,
                    0,
                    video_mode.width,
                    video_mode.height,
                    None,
                );

                log::debug!("Fullscreen turned on (Monitor: {})", monitor_name);
            });
        } else {
            let last_pos = self.last_pos.borrow();
            let last_size = self.last_size.borrow();
            self.window.borrow_mut().set_monitor(
                WindowMode::Windowed,
                last_pos.0,
                last_pos.1,
                last_size.0 as u32,
                last_size.1 as u32,
                None,
            );

            log::debug!("Fullscreen turned off");
        }

        self.is_fullscreen = fullscreen;
        self.ignore_cursor_delta = true;
    }

    pub fn set_grabbed(&mut self, grabbed: bool) {
        if self.is_grabbed == grabbed {
            return;
        }

        let mut window = self.window.borrow_mut();
        window.set_cursor_mode(if grabbed {
            CursorMode::Disabled
        } else {
            CursorMode::Normal
        });

        if self.glfw.supports_raw_motion() {
            window.set_raw_mouse_motion(self.is_grabbed);
        } else {
            log::warn!("Raw mouse motion not supported");
        }

        if grabbed {
            log::debug!("Cursor grabbing turned on");
        } else {
            log::debug!("Cursor grabbing turned off");
        }

        self.is_grabbed = grabbed;
        self.ignore_cursor_delta = true;
    }

    pub fn context(&self) -> glfw::RenderContext {
        self.window.borrow_mut().render_context()
    }
}

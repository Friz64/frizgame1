use super::LogicProcessor;
use crate::Game;
use glam::{const_vec3, Mat4, Vec3};
use glfw::{Action, Key, WindowEvent};

const UP_VEC: Vec3 = const_vec3!([0.0, 1.0, 0.0]);

#[derive(Debug)]
pub struct PlayerCamera {
    forwards: bool,
    backwards: bool,
    left: bool,
    right: bool,

    speed: f32,
    pos: Vec3,
    yaw: f32,
    pitch: f32,
    /// Normalized vector pointing in the direction of `yaw` and `pitch`
    direction: Vec3,
}

impl Default for PlayerCamera {
    fn default() -> PlayerCamera {
        PlayerCamera {
            forwards: false,
            backwards: false,
            left: false,
            right: false,
            speed: 1.0,
            pos: Vec3::new(0.0, 0.0, 0.0),
            yaw: 0.0,
            pitch: 0.0,
            direction: Vec3::new(0.0, 0.0, -1.0),
        }
    }
}

impl PlayerCamera {
    pub fn view_projection_matrix(game: &Game, width: u32, height: u32) -> Mat4 {
        let cam = &game.logic.player_cam;

        let view = Mat4::look_at_rh(cam.pos, cam.pos + cam.direction, UP_VEC);

        let aspect_ratio = width as f32 / height as f32;
        let mut projection = Mat4::perspective_infinite_reverse_rh(
            game.config.fov.to_radians(),
            aspect_ratio,
            0.001,
        );
        projection.y_axis.y *= -1.0; // flip y for vulkan

        projection * view
    }

    pub fn pos(&self) -> Vec3 {
        self.pos
    }
}

impl LogicProcessor for PlayerCamera {
    fn process(game: &mut Game) {
        let cam = &mut game.logic.player_cam;

        for event in game.events.iter() {
            match event {
                WindowEvent::Key(key, _scancode, action, _modifiers) => {
                    let active = *action != Action::Release;
                    match key {
                        Key::W => cam.forwards = active,
                        Key::S => cam.backwards = active,
                        Key::A => cam.left = active,
                        Key::D => cam.right = active,
                        _ => (),
                    }
                }
                WindowEvent::Scroll(_dx, dy) => {
                    cam.speed *= (1.0 - dy.abs() as f32 * 0.06).powf(-dy.signum() as f32);
                    cam.speed = cam.speed.max(0.01);
                }
                _ => (),
            }
        }

        if game.grabbed {
            let mut movement = Vec3::ZERO;

            if cam.forwards {
                movement.z += 1.0;
            }

            if cam.backwards {
                movement.z += -1.0;
            }

            if cam.right {
                movement.x += 1.0;
            }

            if cam.left {
                movement.x += -1.0;
            }

            if movement.length_squared() > 1e-6 {
                movement = movement.normalize() * cam.speed * game.delta_time as f32;
            }

            let mouse_sensitivity = game.config.mouse_sensitivity * 0.05;
            cam.yaw += (game.cursor_delta.x * mouse_sensitivity) as f32;
            cam.pitch += (-game.cursor_delta.y * mouse_sensitivity) as f32;
            cam.pitch = cam.pitch.min(89.999).max(-89.999);

            let direction = Vec3::new(
                cam.yaw.to_radians().sin() * cam.pitch.to_radians().cos(),
                cam.pitch.to_radians().sin(),
                -cam.yaw.to_radians().cos() * cam.pitch.to_radians().cos(),
            )
            .normalize();

            let right = Vec3::cross(direction, UP_VEC).normalize();
            let up = Vec3::cross(right, direction).normalize();

            cam.pos += movement.x * right;
            cam.pos += movement.y * up;
            cam.pos += movement.z * direction;
            cam.direction = direction;
        }
    }
}

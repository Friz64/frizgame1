use super::LogicProcessor;
use crate::Game;
use glam::Vec3;

const RADIUS: f32 = 3.0;

#[derive(Default)]
pub struct Light {
    pub pos: Vec3,
}

impl LogicProcessor for Light {
    fn process(game: &mut Game) {
        let elapsed = game.start.elapsed().as_secs_f32();
        game.logic.light.pos = Vec3::new(elapsed.sin() * RADIUS, 2.0, elapsed.cos() * RADIUS);
    }
}

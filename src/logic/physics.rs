use crate::logic::LogicProcessor;
use crate::Game;
use interpolation::{InterpolatedPhysicsTransforms, PhysicsTime};
use rapier3d::dynamics::{
    CCDSolver, IntegrationParameters, JointSet, RigidBody, RigidBodyBuilder, RigidBodyHandle,
    RigidBodySet,
};
use rapier3d::geometry::{BroadPhase, ColliderBuilder, ColliderHandle, ColliderSet, NarrowPhase};
use rapier3d::math::AngVector;
use rapier3d::na::{Isometry3, Vector3};
use rapier3d::pipeline::PhysicsPipeline;

pub struct PhysicsWorld {
    pipeline: PhysicsPipeline,
    gravity: Vector3<f32>,
    integration_parameters: IntegrationParameters,
    broad_phase: BroadPhase,
    narrow_phase: NarrowPhase,
    bodies: RigidBodySet,
    colliders: ColliderSet,
    joints: JointSet,
    ccd_solver: CCDSolver,

    objects: Vec<Object>,
    time: PhysicsTime,
}

impl Default for PhysicsWorld {
    fn default() -> Self {
        let mut world = Self::new_empty(1.0 / 40.0);

        world.add_object(
            RigidBodyBuilder::new_static()
                .translation(0.0, 0.0, 0.0)
                .build(),
            ColliderType::Ground,
            None,
        );

        let size = 2.0;
        for i in 0..1000 {
            world.add_object(
                RigidBodyBuilder::new_dynamic()
                    .translation(0.0, 10.0 + size * i as f32 * 2.3, 0.0)
                    .angvel(AngVector::new(0.1, -0.4, 0.2) * 6.0)
                    .build(),
                ColliderType::Cube { size },
                Some(500.0),
            );
        }

        world
    }
}

impl PhysicsWorld {
    pub fn new_empty(timestep: f32) -> Self {
        let mut integration_parameters = IntegrationParameters::default();
        integration_parameters.dt = timestep;

        let colliders = ColliderSet::new();
        let bodies = RigidBodySet::new();

        Self {
            pipeline: PhysicsPipeline::new(),
            gravity: Vector3::new(0.0, -9.81, 0.0),
            integration_parameters,
            broad_phase: BroadPhase::new(),
            narrow_phase: NarrowPhase::new(),
            bodies,
            colliders,
            joints: JointSet::new(),
            ccd_solver: CCDSolver::new(),

            objects: vec![],
            time: PhysicsTime {
                computed_steps: 0,
                timestep,
                last_update_count: 0,
                step_time_counter: 0.0,
            },
        }
    }

    pub fn add_object(
        &mut self,
        rigidbody: RigidBody,
        collider_type: ColliderType,
        collider_density: Option<f32>,
    ) -> Object {
        let transform =
            InterpolatedPhysicsTransforms::new_non_moving(*rigidbody.position(), &self.time);
        let cached_frame_transform = *rigidbody.position();

        let rigidbody_handle = self.bodies.insert(rigidbody);

        let object = Object {
            transform,
            cached_frame_transform,
            rigidbody_handle,
            collider_handle: self.colliders.insert(
                collider_type.generate(collider_density).build(),
                rigidbody_handle,
                &mut self.bodies,
            ),
            collider_type,
        };

        self.objects.push(object);

        object
    }

    pub fn objects(&self) -> &[Object] {
        &self.objects
    }
}

/// Collider presets for the game
#[derive(Copy, Clone, Debug)]
pub enum ColliderType {
    /// Y-Up
    Ground,
    Cube {
        size: f32,
    }, // Other, Cylinder, etc..
}

impl ColliderType {
    pub fn generate(&self, density: Option<f32>) -> ColliderBuilder {
        let mut res = match self {
            ColliderType::Cube { size } => {
                ColliderBuilder::cuboid(size * 0.5, size * 0.5, size * 0.5)
            }
            ColliderType::Ground => ColliderBuilder::cuboid(10000.0, 0.025, 10000.0),
        };

        if let Some(density) = density {
            res = res.density(density);
        }

        res
    }

    pub fn graphical_model_name(&self) -> &str {
        match self {
            ColliderType::Ground => "grid",
            ColliderType::Cube { .. } => "cube",
        }
    }

    pub fn graphical_model_scale(&self) -> Vector3<f32> {
        match self {
            ColliderType::Ground => Vector3::repeat(1.0),
            ColliderType::Cube { size } => Vector3::repeat(*size * 0.5),
        }
    }
}

#[derive(Copy, Clone)]
pub struct Object {
    rigidbody_handle: RigidBodyHandle,
    collider_handle: ColliderHandle,

    collider_type: ColliderType,

    transform: InterpolatedPhysicsTransforms,
    /// The object's interpolated transform after its PhysicsWorld was stepped
    cached_frame_transform: Isometry3<f32>,
}

impl Object {
    pub fn cached_frame_transform(&self) -> &Isometry3<f32> {
        &self.cached_frame_transform
    }

    pub fn collider_type(&self) -> &ColliderType {
        &self.collider_type
    }
}

impl LogicProcessor for PhysicsWorld {
    fn process(game: &mut Game) {
        let physics = &mut game.logic.physics;
        let dt = physics.integration_parameters.dt as f64;

        let time = &mut physics.time;

        time.step_time_counter += game.delta_time * game.physics_time_scale;

        let mut stepped = false;

        time.last_update_count = 0;

        while time.step_time_counter >= dt {
            physics.pipeline.step(
                &physics.gravity,
                &physics.integration_parameters,
                &mut physics.broad_phase,
                &mut physics.narrow_phase,
                &mut physics.bodies,
                &mut physics.colliders,
                &mut physics.joints,
                &mut physics.ccd_solver,
                &(),
                &(),
            );

            time.step_time_counter -= dt;

            time.computed_steps += 1;
            time.last_update_count += 1;

            stepped = true;

            for o in physics.objects.iter_mut() {
                o.transform.step_after_physics(
                    *physics
                        .bodies
                        .get(o.rigidbody_handle)
                        .expect("invalid physics object rigidbody handle")
                        .position(),
                    &*time,
                );
            }
        }

        for o in physics.objects.iter_mut() {
            o.cached_frame_transform = o.transform.interpolate(&physics.time);
        }
    }
}

pub mod interpolation {
    use rapier3d::na::Isometry3;

    #[derive(Default, Copy, Clone)]
    pub struct PhysicsTime {
        pub timestep: f32,

        /// Used to track a fixed timestep for physics worlds
        pub step_time_counter: f64,

        /// How many physics steps were performed last time
        pub last_update_count: usize,

        /// How many physics steps were performed in total
        pub computed_steps: u64,
    }

    /// Interpolates the transforms of a rigidbody between earlier and now. A fixed physics timestep is assumed.
    pub fn interpolate_physics_transforms(
        last: &Isometry3<f32>,
        current: &Isometry3<f32>,
        physics_time: &PhysicsTime,
    ) -> Isometry3<f32> {
        let factor = physics_time.step_time_counter as f32 / physics_time.timestep;
        Isometry3 {
            translation: (last.translation.vector
                + factor * (current.translation.vector - last.translation.vector))
                .into(),
            rotation: last.rotation.slerp(&current.rotation, factor),
        }
    }

    /// Represents the transform of a model of a physics body. Using [`PhysicsTime`], it is guaranteed that
    /// interpolation gives correct transforms for this time.
    ///
    /// It is assumed that [`InterpolatedPhysicsTransforms::interpolate`]
    /// is called more than once per physics step to make use of caching interpolated transforms.
    ///
    /// [`InterpolatedPhysicsTransforms::step`] may only be called once per physics step.
    #[derive(Copy, Clone)]
    pub struct InterpolatedPhysicsTransforms {
        /// Value of [PhysicsTime::step] during last step. Used to ensure consistent updates each frame
        last_computed_steps: u64,
        /// Transform of the body before the last physics step
        last_transform: Isometry3<f32>,
        /// Transform of the body after the last physics step
        current_transform: Isometry3<f32>,
    }

    impl InterpolatedPhysicsTransforms {
        #[inline]
        pub fn new_non_moving(
            current_transform: Isometry3<f32>,
            physics_time: &PhysicsTime,
        ) -> Self {
            Self {
                last_transform: current_transform,

                current_transform,
                last_computed_steps: physics_time.computed_steps,
            }
        }

        #[inline]
        pub fn interpolate(&self, physics_time: &PhysicsTime) -> Isometry3<f32> {
            interpolate_physics_transforms(
                &self.last_transform,
                &self.current_transform,
                physics_time,
            )
        }

        #[inline]
        pub fn last_transform(&self) -> Isometry3<f32> {
            self.last_transform
        }

        #[inline]
        pub fn current_transform(&self) -> Isometry3<f32> {
            self.current_transform
        }

        /// This should be called *before* the physics step with the body's new transform.
        ///
        /// This function should only be used for bodys which update their position in operations shortly before a
        /// world's physics step
        #[inline]
        pub(crate) fn step_before_physics(
            &mut self,
            new_transform: Isometry3<f32>,
            physics_time: &PhysicsTime,
        ) {
            assert_eq!(
                physics_time.computed_steps, self.last_computed_steps,
                "Interpolated physics transform was not updated after physics step (right value) but later in physics step (left value)."
            );
            self.last_computed_steps = self.last_computed_steps.wrapping_add(1);

            self.last_transform = self.current_transform;
            self.current_transform = new_transform;
        }

        /// This should be called *after* the physics step with the body's new transform.
        #[inline]
        pub fn step_after_physics(
            &mut self,
            new_transform: Isometry3<f32>,
            physics_time: &PhysicsTime,
        ) {
            self.last_computed_steps = self.last_computed_steps.wrapping_add(1);
            assert_eq!(
                physics_time.computed_steps, self.last_computed_steps,
                "Interpolated physics transform was not updated after physics step (right value) but later in physics step (left value)."
            );

            self.last_transform = self.current_transform;
            self.current_transform = new_transform;
        }
    }
}

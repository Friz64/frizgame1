pub mod light;
pub mod physics;
pub mod player_cam;

use self::{light::Light, player_cam::PlayerCamera};
use crate::logic::physics::PhysicsWorld;
use crate::Game;

trait LogicProcessor {
    fn process(game: &mut Game);
}

#[derive(Default)]
pub struct Logic {
    pub light: Light,
    pub player_cam: PlayerCamera,
    pub physics: PhysicsWorld,
}

impl Logic {
    pub fn new() -> Logic {
        Logic::default()
    }

    pub fn process(game: &mut Game) {
        Light::process(game);
        PlayerCamera::process(game);
        PhysicsWorld::process(game);
    }
}

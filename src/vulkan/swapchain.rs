use super::vk_context::VkContext;
use crate::config::Framerate;
use color_eyre::{
    eyre::{Result, WrapErr},
    Report,
};
use erupt::vk;

#[derive(Clone, Copy)]
pub struct SwapchainImage {
    index: u32,
    _image: vk::Image,
    view: vk::ImageView,
}

impl SwapchainImage {
    fn new(
        ctx: &VkContext,
        i: usize,
        image: vk::Image,
        format: vk::Format,
    ) -> Result<SwapchainImage> {
        let view = ctx
            .create_image_view(image, format, vk::ImageAspectFlags::COLOR)
            .wrap_err("Failed to create swapchain image view")?;

        Ok(SwapchainImage {
            index: i as u32,
            _image: image,
            view,
        })
    }

    fn destroy(&self, ctx: &VkContext) {
        unsafe {
            ctx.device().destroy_image_view(Some(self.view), None);
        }
    }

    pub fn index(&self) -> usize {
        self.index as usize
    }

    pub fn view(&self) -> vk::ImageView {
        self.view
    }
}

pub struct Swapchain {
    swapchain: vk::SwapchainKHR,
    images: Vec<SwapchainImage>,
    surface_format: vk::SurfaceFormatKHR,
    current_extent: vk::Extent2D,
    was_recreated: bool,
}

impl Swapchain {
    pub fn new(
        ctx: &VkContext,
        framerate: &Framerate,
        old: Option<&Swapchain>,
    ) -> Result<Swapchain> {
        let present_mode = match framerate {
            Framerate::Unlimited | Framerate::Limited(_) => {
                let present_modes = ctx.physical().present_modes();
                let mut target = vk::PresentModeKHR::MAILBOX_KHR;
                if !present_modes.contains(&target) {
                    target = vk::PresentModeKHR::IMMEDIATE_KHR;
                    if !present_modes.contains(&target) {
                        target = vk::PresentModeKHR::FIFO_KHR;
                        log::warn!("Both MAILBOX and IMMEDIATE unsupported, falling back to FIFO");
                    } else {
                        log::warn!("MAILBOX unsupported, falling back to IMMEDIATE");
                    }
                }

                target
            }
            Framerate::Sync => vk::PresentModeKHR::FIFO_KHR,
        };

        let surface_capabilities = unsafe {
            ctx.instance()
                .get_physical_device_surface_capabilities_khr(
                    ctx.physical().handle(),
                    ctx.surface(),
                )
                .result()
                .wrap_err("Failed to get surface capabilities")?
        };

        let mut min_image_count = surface_capabilities.min_image_count + 1;
        if surface_capabilities.max_image_count != 0 {
            min_image_count = min_image_count.min(surface_capabilities.max_image_count);
        }

        let surface_formats = ctx.physical().surface_formats();
        let surface_format = *surface_formats
            .iter()
            .find(|surface_format| {
                surface_format.format == vk::Format::B8G8R8A8_UNORM
                    && surface_format.color_space == vk::ColorSpaceKHR::SRGB_NONLINEAR_KHR
            })
            .unwrap_or_else(|| &surface_formats[0]);

        let create_info = vk::SwapchainCreateInfoKHRBuilder::new()
            .surface(ctx.surface())
            .min_image_count(min_image_count)
            .image_format(surface_format.format)
            .image_color_space(surface_format.color_space)
            .image_extent(surface_capabilities.current_extent)
            .image_array_layers(1)
            .image_usage(vk::ImageUsageFlags::COLOR_ATTACHMENT)
            .image_sharing_mode(vk::SharingMode::EXCLUSIVE)
            .pre_transform(surface_capabilities.current_transform)
            .composite_alpha(vk::CompositeAlphaFlagBitsKHR::OPAQUE_KHR)
            .present_mode(present_mode)
            .clipped(true)
            .old_swapchain(old.map(|swapchain| swapchain.swapchain).unwrap_or_default());

        let swapchain = unsafe {
            ctx.device()
                .create_swapchain_khr(&create_info, None)
                .result()
                .wrap_err("Failed to create swapchain")?
        };

        let image_handles = unsafe {
            ctx.device()
                .get_swapchain_images_khr(swapchain, None)
                .result()
                .wrap_err("Failed to get swapchain images")?
        };

        let images = image_handles
            .into_iter()
            .enumerate()
            .map(|(i, image)| SwapchainImage::new(ctx, i, image, surface_format.format))
            .collect::<Result<Vec<_>>>()?;

        log::info!(
            "Created at {}x{} with {:?}",
            surface_capabilities.current_extent.width,
            surface_capabilities.current_extent.height,
            framerate
        );

        Ok(Swapchain {
            swapchain,
            images,
            surface_format,
            current_extent: surface_capabilities.current_extent,
            was_recreated: false,
        })
    }

    pub fn destroy(&self, ctx: &VkContext) {
        unsafe {
            for image in &self.images {
                image.destroy(ctx);
            }

            ctx.device()
                .destroy_swapchain_khr(Some(self.swapchain), None);
        }
    }

    pub fn recreate(&mut self, ctx: &VkContext, framerate: &Framerate) -> Result<()> {
        unsafe {
            ctx.device()
                .queue_wait_idle(ctx.queue())
                .result()
                .wrap_err("Failed to wait for queue idle")?;
        }

        let replacement = Swapchain::new(ctx, framerate, Some(self))
            .wrap_err("Failed to create replacement swapchain")?;
        self.destroy(ctx);
        *self = replacement;
        self.was_recreated = true;
        Ok(())
    }

    pub fn surface_format(&self) -> vk::SurfaceFormatKHR {
        self.surface_format
    }

    pub fn images(&self) -> &[SwapchainImage] {
        &self.images
    }

    pub fn current_extent(&self) -> vk::Extent2D {
        self.current_extent
    }

    pub fn next_image(
        &mut self,
        ctx: &VkContext,
        framerate: &Framerate,
        signal_semaphore: vk::Semaphore,
    ) -> Result<(SwapchainImage, bool)> {
        let result = unsafe {
            ctx.device().acquire_next_image_khr(
                self.swapchain,
                u64::MAX,
                Some(signal_semaphore),
                None,
            )
        };

        let was_recreated = self.was_recreated;
        self.was_recreated = false;
        match result.raw {
            vk::Result::SUCCESS => Ok((self.images[result.value.unwrap() as usize], was_recreated)),
            vk::Result::ERROR_OUT_OF_DATE_KHR | vk::Result::SUBOPTIMAL_KHR => {
                self.recreate(ctx, framerate)?;
                self.next_image(ctx, framerate, signal_semaphore)
            }
            err => Err(Report::new(err)),
        }
    }

    pub fn present(
        &self,
        ctx: &VkContext,
        image: &SwapchainImage,
        wait_semaphores: &[vk::Semaphore],
    ) -> Result<()> {
        let swapchains = &[self.swapchain];
        let image_indices = &[image.index];
        let present_info = vk::PresentInfoKHRBuilder::new()
            .wait_semaphores(wait_semaphores)
            .swapchains(swapchains)
            .image_indices(image_indices);

        let result = unsafe { ctx.device().queue_present_khr(ctx.queue(), &present_info) };
        match result.raw {
            vk::Result::SUCCESS
            | vk::Result::ERROR_OUT_OF_DATE_KHR
            | vk::Result::SUBOPTIMAL_KHR => Ok(()),
            err => Err(Report::new(err)),
        }
    }

    pub fn set_dynamic_viewport(&self, ctx: &VkContext, command_buffer: vk::CommandBuffer) {
        let current_extent = self.current_extent();
        unsafe {
            ctx.device().cmd_set_viewport(
                command_buffer,
                0,
                &[vk::ViewportBuilder::new()
                    .x(0.0)
                    .y(0.0)
                    .width(current_extent.width as _)
                    .height(current_extent.height as _)
                    .min_depth(0.0)
                    .max_depth(1.0)],
            );

            ctx.device().cmd_set_scissor(
                command_buffer,
                0,
                &[vk::Rect2D {
                    offset: vk::Offset2D { x: 0, y: 0 },
                    extent: current_extent,
                }
                .into_builder()],
            );
        }
    }
}

mod test_pipeline;

use crate::{
    vulkan::{
        swapchain::{Swapchain, SwapchainImage},
        vk_context::{Allocation, VkContext},
    },
    Game,
};
use color_eyre::{
    eyre::{Result, WrapErr},
    Report,
};
use erupt::vk;
use glam::Mat4;
use test_pipeline::TestPipeline;

const DEPTH_IMAGE_FORMAT: vk::Format = vk::Format::D32_SFLOAT;

pub struct MainPass {
    render_pass: vk::RenderPass,
    test_pipeline: TestPipeline,
    framebuffers: Vec<vk::Framebuffer>,
    depth_image: Option<Allocation<vk::Image>>,
    depth_image_view: Option<vk::ImageView>,
}

impl MainPass {
    pub fn new(ctx: &VkContext, swapchain: &Swapchain) -> Result<MainPass> {
        let color_attachment = vk::AttachmentDescriptionBuilder::new()
            .format(swapchain.surface_format().format)
            .samples(vk::SampleCountFlagBits::_1)
            .load_op(vk::AttachmentLoadOp::CLEAR)
            .store_op(vk::AttachmentStoreOp::STORE)
            .stencil_load_op(vk::AttachmentLoadOp::DONT_CARE)
            .stencil_store_op(vk::AttachmentStoreOp::DONT_CARE)
            .initial_layout(vk::ImageLayout::UNDEFINED)
            .final_layout(vk::ImageLayout::PRESENT_SRC_KHR);

        let depth_attachment = vk::AttachmentDescriptionBuilder::new()
            .format(DEPTH_IMAGE_FORMAT)
            .samples(vk::SampleCountFlagBits::_1)
            .load_op(vk::AttachmentLoadOp::CLEAR)
            .store_op(vk::AttachmentStoreOp::DONT_CARE)
            .stencil_load_op(vk::AttachmentLoadOp::DONT_CARE)
            .stencil_store_op(vk::AttachmentStoreOp::DONT_CARE)
            .initial_layout(vk::ImageLayout::UNDEFINED)
            .final_layout(vk::ImageLayout::DEPTH_STENCIL_ATTACHMENT_OPTIMAL);

        let subpass_color_attachments = &[vk::AttachmentReferenceBuilder::new()
            .attachment(0)
            .layout(vk::ImageLayout::COLOR_ATTACHMENT_OPTIMAL)];
        let subpass_depth_stencil_attachment = vk::AttachmentReferenceBuilder::new()
            .attachment(1)
            .layout(vk::ImageLayout::DEPTH_STENCIL_ATTACHMENT_OPTIMAL);
        let subpass = vk::SubpassDescriptionBuilder::new()
            .pipeline_bind_point(vk::PipelineBindPoint::GRAPHICS)
            .color_attachments(subpass_color_attachments)
            .depth_stencil_attachment(&subpass_depth_stencil_attachment);

        // This dependency is required to prevent a race condition, which happens when
        // the *implicit* sub-pass dependency from EXTERNAL -> 0 performs
        // the image layout transition too early because its srcStageMask
        // is set to TOP_OF_PIPE, while the image is only actually acquired
        // in the COLOR_ATTACHMENT_OUTPUT stage according to the waitDstStageMask
        // of the queue submission wait semaphore.
        let dependency = vk::SubpassDependencyBuilder::new()
            .src_subpass(vk::SUBPASS_EXTERNAL)
            .dst_subpass(0)
            .src_stage_mask(
                vk::PipelineStageFlags::COLOR_ATTACHMENT_OUTPUT
                    | vk::PipelineStageFlags::LATE_FRAGMENT_TESTS,
            )
            .src_access_mask(vk::AccessFlags::empty())
            .dst_stage_mask(
                vk::PipelineStageFlags::COLOR_ATTACHMENT_OUTPUT
                    | vk::PipelineStageFlags::EARLY_FRAGMENT_TESTS
                    | vk::PipelineStageFlags::LATE_FRAGMENT_TESTS,
            )
            .dst_access_mask(
                vk::AccessFlags::COLOR_ATTACHMENT_WRITE
                    | vk::AccessFlags::DEPTH_STENCIL_ATTACHMENT_WRITE,
            );

        let render_pass_attachments = &[color_attachment, depth_attachment];
        let render_pass_subpasses = &[subpass];
        let render_pass_dependencies = &[dependency];
        let render_pass_info = vk::RenderPassCreateInfoBuilder::new()
            .attachments(render_pass_attachments)
            .subpasses(render_pass_subpasses)
            .dependencies(render_pass_dependencies);

        let render_pass = unsafe {
            ctx.device()
                .create_render_pass(&render_pass_info, None)
                .result()
                .wrap_err("Failed to create render pass")?
        };

        let test_pipeline =
            TestPipeline::new(ctx, render_pass, 0).wrap_err("Failed to create test pipeline")?;

        let mut main_pass = MainPass {
            render_pass,
            test_pipeline,
            framebuffers: Vec::new(),
            depth_image: None,
            depth_image_view: None,
        };

        main_pass.create_depth_image(ctx, swapchain)?;
        main_pass.create_framebuffers(ctx, swapchain, render_pass)?;
        Ok(main_pass)
    }

    fn create_framebuffers(
        &mut self,
        ctx: &VkContext,
        swapchain: &Swapchain,
        render_pass: vk::RenderPass,
    ) -> Result<()> {
        let depth_image_view = self.depth_image_view.unwrap();
        let current_extent = swapchain.current_extent();
        self.framebuffers = swapchain
            .images()
            .iter()
            .map(|image| {
                let attachments = &[image.view(), depth_image_view];
                let framebuffer_info = vk::FramebufferCreateInfoBuilder::new()
                    .render_pass(render_pass)
                    .attachments(attachments)
                    .width(current_extent.width)
                    .height(current_extent.height)
                    .layers(1);

                unsafe {
                    ctx.device()
                        .create_framebuffer(&framebuffer_info, None)
                        .result()
                        .map_err(Report::new)
                }
            })
            .collect::<Result<Vec<vk::Framebuffer>>>()
            .wrap_err("Failed to create a swapchain framebuffer")?;

        Ok(())
    }

    fn destroy_framebuffers(&self, ctx: &VkContext) {
        for &framebuffer in &self.framebuffers {
            unsafe {
                ctx.device().destroy_framebuffer(Some(framebuffer), None);
            }
        }
    }

    fn create_depth_image(&mut self, ctx: &VkContext, swapchain: &Swapchain) -> Result<()> {
        let depth_image = ctx
            .alloc_image(
                swapchain.current_extent(),
                DEPTH_IMAGE_FORMAT,
                vk::ImageTiling::OPTIMAL,
                vk::ImageUsageFlags::DEPTH_STENCIL_ATTACHMENT,
                gpu_alloc::UsageFlags::FAST_DEVICE_ACCESS,
            )
            .wrap_err("Failed to create depth image")?;

        let depth_image_view = ctx
            .create_image_view(
                depth_image.object(),
                DEPTH_IMAGE_FORMAT,
                vk::ImageAspectFlags::DEPTH,
            )
            .context("Failed to create depth image view")?;

        self.depth_image = Some(depth_image);
        self.depth_image_view = Some(depth_image_view);
        Ok(())
    }

    fn destroy_depth_image(&mut self, ctx: &VkContext) {
        ctx.dealloc_image(self.depth_image.take().unwrap());
        unsafe {
            ctx.device()
                .destroy_image_view(Some(self.depth_image_view.take().unwrap()), None);
        }
    }

    pub fn run(
        &mut self,
        game: &Game,
        ctx: &VkContext,
        command_buffer: vk::CommandBuffer,
        swapchain: &Swapchain,
        swapchain_image: &SwapchainImage,
        swapchain_recreated: bool,
        objects: &[(&str, Mat4)],
    ) -> Result<()> {
        if swapchain_recreated {
            self.destroy_depth_image(ctx);
            self.destroy_framebuffers(ctx);
            self.create_depth_image(ctx, swapchain)?;
            self.create_framebuffers(ctx, swapchain, self.render_pass)?;
        }

        let render_pass_info = vk::RenderPassBeginInfoBuilder::new()
            .render_pass(self.render_pass)
            .framebuffer(self.framebuffers[swapchain_image.index()])
            .render_area(vk::Rect2D {
                offset: vk::Offset2D { x: 0, y: 0 },
                extent: swapchain.current_extent(),
            })
            .clear_values(&[
                vk::ClearValue {
                    color: vk::ClearColorValue {
                        float32: [0.0, 0.0, 0.0, 1.0],
                    },
                },
                vk::ClearValue {
                    depth_stencil: vk::ClearDepthStencilValue {
                        depth: 0.0,
                        stencil: 0,
                    },
                },
            ]);

        unsafe {
            ctx.device().cmd_begin_render_pass(
                command_buffer,
                &render_pass_info,
                vk::SubpassContents::INLINE,
            );

            self.test_pipeline
                .run(game, ctx, command_buffer, swapchain, objects);
            ctx.device().cmd_end_render_pass(command_buffer);
        }

        Ok(())
    }

    pub fn destroy(&mut self, ctx: &VkContext) {
        self.test_pipeline.destroy(ctx);
        self.destroy_depth_image(ctx);
        self.destroy_framebuffers(ctx);
        unsafe {
            ctx.device()
                .destroy_render_pass(Some(self.render_pass), None);
        }
    }
}

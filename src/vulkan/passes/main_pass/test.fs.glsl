#version 450

layout(location = 0) in vec3 position_in;
layout(location = 1) in vec3 normal_in;

layout(location = 0) out vec4 color_out;

layout(push_constant) uniform PushConstants {
    layout(offset = 64) vec3 light_pos;
    vec3 view_pos;
};

void main() {
    //vec3 base_color = vec3(1.0, 1.0, 1.0);
    vec3 base_color = normal_in * 0.5 + vec3(0.5);
    vec3 light_color = vec3(0.3);

    vec3 ambient = 0.05 * base_color;

    vec3 light_dir = normalize(light_pos - position_in);
    vec3 normal = normalize(normal_in);
    float diff = max(dot(light_dir, normal), 0.0);
    vec3 diffuse = diff * base_color;

    vec3 view_dir = normalize(view_pos - position_in);
    vec3 halfway_dir = normalize(light_dir + view_dir);
    float spec = pow(max(dot(normal, halfway_dir), 0.0), 256.0);
    vec3 specular = light_color * spec;

    color_out = vec4(ambient + diffuse + specular, 1.0);
}

#version 450
#extension GL_EXT_debug_printf : enable

layout(location = 0) in vec3 position_in;
layout(location = 1) in vec3 normal_in;

layout(location = 0) out vec3 position_out;
layout(location = 1) out vec3 normal_out;

layout(push_constant) uniform PushConstants {
    mat4 mvp_matrix;
};

void main() {
    gl_Position = mvp_matrix * vec4(position_in, 1.0);
    position_out = position_in;
    normal_out = normal_in;
}

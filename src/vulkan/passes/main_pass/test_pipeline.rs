use crate::{
    include_res,
    logic::player_cam::PlayerCamera,
    vulkan::{
        swapchain::Swapchain,
        vk_context::{Allocation, VkContext},
    },
    Game,
};
use bytemuck::Pod;
use color_eyre::eyre::{Result, WrapErr};
use erupt::{cstr, vk};
use glam::{Mat4, Vec3, Vec3A};
use memoffset::offset_of;
use obj::Obj;
use std::collections::HashMap;
use std::convert::TryInto;
use std::marker::PhantomData;
use std::{ffi::CStr, mem};

#[derive(Copy, Clone)]
struct Vertex {
    position: Vec3,
    normal: Vec3,
}

unsafe impl bytemuck::Zeroable for Vertex {}
unsafe impl bytemuck::Pod for Vertex {}

#[derive(Copy, Clone)]
#[repr(C, align(16))]
struct VertexPushConstants {
    mvp_matrix: Mat4,
}

unsafe impl bytemuck::Zeroable for VertexPushConstants {}
unsafe impl bytemuck::Pod for VertexPushConstants {}

#[derive(Copy, Clone)]
#[repr(C, align(16))]
struct FragmentPushConstants {
    light_pos: Vec3A,
    view_pos: Vec3A,
}

unsafe impl bytemuck::Zeroable for FragmentPushConstants {}
unsafe impl bytemuck::Pod for FragmentPushConstants {}

pub struct TestPipeline {
    pipeline: vk::Pipeline,
    pipeline_layout: vk::PipelineLayout,

    models: HashMap<String, AllocatedModel<Vertex>>,
}

impl TestPipeline {
    pub fn new(ctx: &VkContext, render_pass: vk::RenderPass, subpass: u32) -> Result<TestPipeline> {
        let entry_point = unsafe { CStr::from_ptr(cstr!("main")) };
        let vertex_module = ctx.create_shader_module(include_bytes!("test.vs.spv"))?;
        let fragment_module = ctx.create_shader_module(include_bytes!("test.fs.spv"))?;
        let shader_stages = &[
            vk::PipelineShaderStageCreateInfoBuilder::new()
                .stage(vk::ShaderStageFlagBits::VERTEX)
                .module(vertex_module)
                .name(&entry_point),
            vk::PipelineShaderStageCreateInfoBuilder::new()
                .stage(vk::ShaderStageFlagBits::FRAGMENT)
                .module(fragment_module)
                .name(&entry_point),
        ];

        let vertex_input_binding_descriptions = &[vk::VertexInputBindingDescriptionBuilder::new()
            .binding(0)
            .stride(mem::size_of::<Vertex>() as _)
            .input_rate(vk::VertexInputRate::VERTEX)];
        let vertex_input_attribute_descriptions = &[
            vk::VertexInputAttributeDescriptionBuilder::new()
                .binding(0)
                .location(0)
                .format(vk::Format::R32G32B32_SFLOAT)
                .offset(offset_of!(Vertex, position) as _),
            vk::VertexInputAttributeDescriptionBuilder::new()
                .binding(0)
                .location(1)
                .format(vk::Format::R32G32B32_SFLOAT)
                .offset(offset_of!(Vertex, normal) as _),
        ];

        let vertex_input_info = vk::PipelineVertexInputStateCreateInfoBuilder::new()
            .vertex_binding_descriptions(vertex_input_binding_descriptions)
            .vertex_attribute_descriptions(vertex_input_attribute_descriptions);

        let input_assembly = vk::PipelineInputAssemblyStateCreateInfoBuilder::new()
            .topology(vk::PrimitiveTopology::TRIANGLE_LIST)
            .primitive_restart_enable(false);

        let mut viewport_state = vk::PipelineViewportStateCreateInfo::default();
        viewport_state.viewport_count = 1;
        viewport_state.scissor_count = 1;

        let dynamic_state = vk::PipelineDynamicStateCreateInfoBuilder::new()
            .dynamic_states(&[vk::DynamicState::VIEWPORT, vk::DynamicState::SCISSOR]);

        let rasterizer = vk::PipelineRasterizationStateCreateInfoBuilder::new()
            .depth_clamp_enable(false)
            .rasterizer_discard_enable(false)
            .polygon_mode(vk::PolygonMode::FILL)
            .line_width(1.0)
            .cull_mode(vk::CullModeFlags::FRONT)
            .front_face(vk::FrontFace::CLOCKWISE)
            .depth_bias_enable(false);

        let multisampling = vk::PipelineMultisampleStateCreateInfoBuilder::new()
            .sample_shading_enable(false)
            .rasterization_samples(vk::SampleCountFlagBits::_1);

        let depth_stencil_info = vk::PipelineDepthStencilStateCreateInfoBuilder::new()
            .depth_test_enable(true)
            .depth_write_enable(true)
            .depth_compare_op(vk::CompareOp::GREATER)
            .stencil_test_enable(false);

        let color_blend_attachments = &[vk::PipelineColorBlendAttachmentStateBuilder::new()
            .color_write_mask(
                vk::ColorComponentFlags::R
                    | vk::ColorComponentFlags::G
                    | vk::ColorComponentFlags::B
                    | vk::ColorComponentFlags::A,
            )
            .blend_enable(false)];

        let color_blending = vk::PipelineColorBlendStateCreateInfoBuilder::new()
            .logic_op_enable(false)
            .attachments(color_blend_attachments);

        let pipeline_layout = ctx.create_pipeline_layout(
            &[],
            &[
                vk::PushConstantRangeBuilder::new()
                    .stage_flags(vk::ShaderStageFlags::VERTEX)
                    .offset(0)
                    .size(mem::size_of::<VertexPushConstants>() as _),
                vk::PushConstantRangeBuilder::new()
                    .stage_flags(vk::ShaderStageFlags::FRAGMENT)
                    .offset(mem::size_of::<VertexPushConstants>() as _)
                    .size(mem::size_of::<FragmentPushConstants>() as _),
            ],
        )?;

        let pipeline_info = vk::GraphicsPipelineCreateInfoBuilder::new()
            .stages(shader_stages)
            .vertex_input_state(&vertex_input_info)
            .input_assembly_state(&input_assembly)
            .viewport_state(&viewport_state)
            .dynamic_state(&dynamic_state)
            .rasterization_state(&rasterizer)
            .multisample_state(&multisampling)
            .depth_stencil_state(&depth_stencil_info)
            .color_blend_state(&color_blending)
            .layout(pipeline_layout)
            .render_pass(render_pass)
            .subpass(subpass);
        let pipeline = ctx.create_graphics_pipeline(pipeline_info, "Test Pipeline")?;

        unsafe {
            ctx.device()
                .destroy_shader_module(Some(vertex_module), None);

            ctx.device()
                .destroy_shader_module(Some(fragment_module), None);
        }

        let mut models = HashMap::new();

        models.insert(
            "grid".to_owned(),
            AllocatedModel::from_obj(ctx, include_res!("frizgame1grid.obj"))
                .wrap_err("failed to create model from obj")?,
        );

        models.insert(
            "cube".to_owned(),
            AllocatedModel::from_obj(ctx, include_res!("cube.obj"))
                .wrap_err("failed to create model from obj")?,
        );

        Ok(TestPipeline {
            pipeline,
            pipeline_layout,
            models,
        })
    }

    pub fn run(
        &mut self,
        game: &Game,
        ctx: &VkContext,
        command_buffer: vk::CommandBuffer,
        swapchain: &Swapchain,
        objects: &[(&str, Mat4)],
    ) {
        let vk::Extent2D { width, height } = swapchain.current_extent();
        let view_projection = PlayerCamera::view_projection_matrix(game, width, height);

        unsafe {
            swapchain.set_dynamic_viewport(ctx, command_buffer);

            ctx.device().cmd_bind_pipeline(
                command_buffer,
                vk::PipelineBindPoint::GRAPHICS,
                self.pipeline,
            );
        }

        for (name, transform) in objects {
            let allocated_model = self
                .models
                .get(*name)
                .unwrap_or_else(|| panic!("unknown model name: {}", name));

            let vertex_push_constants = VertexPushConstants {
                mvp_matrix: view_projection * *transform,
            };

            let fragment_push_constants = FragmentPushConstants {
                light_pos: transform
                    .inverse()
                    .transform_point3(game.logic.light.pos)
                    .into(),
                view_pos: transform
                    .inverse()
                    .transform_point3(game.logic.player_cam.pos())
                    .into(),
            };

            unsafe {
                ctx.device().cmd_push_constants(
                    command_buffer,
                    self.pipeline_layout,
                    vk::ShaderStageFlags::VERTEX,
                    0,
                    mem::size_of::<VertexPushConstants>() as _,
                    &vertex_push_constants as *const VertexPushConstants as _,
                );

                ctx.device().cmd_push_constants(
                    command_buffer,
                    self.pipeline_layout,
                    vk::ShaderStageFlags::FRAGMENT,
                    mem::size_of::<VertexPushConstants>() as _,
                    mem::size_of::<FragmentPushConstants>() as _,
                    &fragment_push_constants as *const FragmentPushConstants as _,
                );

                ctx.device().cmd_bind_vertex_buffers(
                    command_buffer,
                    0,
                    &[allocated_model.vertex_buffer.object()],
                    &[0],
                );

                ctx.device().cmd_bind_index_buffer(
                    command_buffer,
                    allocated_model.index_buffer.object(),
                    0,
                    vk::IndexType::UINT32,
                );

                ctx.device().cmd_draw_indexed(
                    command_buffer,
                    allocated_model.index_count,
                    1,
                    0,
                    0,
                    0,
                );
            }
        }
    }

    pub fn destroy(&mut self, ctx: &VkContext) {
        unsafe {
            self.models
                .drain()
                .for_each(|(_, m)| drop(m.deallocate(ctx)));

            ctx.device()
                .destroy_pipeline_layout(Some(self.pipeline_layout), None);

            ctx.device().destroy_pipeline(Some(self.pipeline), None);
        }
    }
}

pub struct AllocatedModel<V, I = u32>
where
    V: Pod,
    I: Pod,
{
    vertex_buffer: Allocation<vk::Buffer>,
    index_buffer: Allocation<vk::Buffer>,
    __index_vertex_types: PhantomData<(I, V)>,

    index_count: u32,
    vertex_count: u32,
}

impl AllocatedModel<Vertex, u32> {
    pub fn from_obj(ctx: &VkContext, obj_bytes: &[u8]) -> Result<Self> {
        let obj: Obj<obj::Vertex, u32> = obj::load_obj(obj_bytes)?;
        let vertices: Vec<_> = obj
            .vertices
            .into_iter()
            .map(|vertex| Vertex {
                position: vertex.position.into(),
                normal: vertex.normal.into(),
            })
            .collect();

        AllocatedModel::new(ctx, obj.indices, vertices)
    }
}

impl<V, I> AllocatedModel<V, I>
where
    V: Pod,
    I: Pod,
{
    pub fn new(ctx: &VkContext, index: Vec<I>, vertices: Vec<V>) -> Result<Self> {
        let vertex_buffer_size = mem::size_of_val(vertices.as_slice());
        let mut staging_vertex_buffer = ctx
            .alloc_buffer(
                vertex_buffer_size,
                vk::BufferUsageFlags::TRANSFER_SRC,
                gpu_alloc::UsageFlags::TRANSIENT
                    | gpu_alloc::UsageFlags::HOST_ACCESS
                    | gpu_alloc::UsageFlags::UPLOAD,
            )
            .wrap_err("Failed to allocate staging vertex buffer")?;
        let target_vertex_buffer = ctx
            .alloc_buffer(
                vertex_buffer_size,
                vk::BufferUsageFlags::TRANSFER_DST | vk::BufferUsageFlags::VERTEX_BUFFER,
                gpu_alloc::UsageFlags::FAST_DEVICE_ACCESS,
            )
            .wrap_err("Failed to allocate target vertex buffer")?;

        let index_buffer_size = mem::size_of_val(index.as_slice());
        let mut staging_index_buffer = ctx
            .alloc_buffer(
                index_buffer_size,
                vk::BufferUsageFlags::TRANSFER_SRC,
                gpu_alloc::UsageFlags::TRANSIENT
                    | gpu_alloc::UsageFlags::HOST_ACCESS
                    | gpu_alloc::UsageFlags::UPLOAD,
            )
            .wrap_err("Failed to allocate staging index buffer")?;
        let target_index_buffer = ctx
            .alloc_buffer(
                index_buffer_size,
                vk::BufferUsageFlags::TRANSFER_DST | vk::BufferUsageFlags::INDEX_BUFFER,
                gpu_alloc::UsageFlags::FAST_DEVICE_ACCESS,
            )
            .wrap_err("Failed to allocate target index buffer")?;

        unsafe {
            staging_vertex_buffer
                .block()
                .write_bytes(ctx.mem_device(), 0, bytemuck::cast_slice(&vertices))
                .wrap_err("Failed to fill staging vertex buffer")?;
            staging_index_buffer
                .block()
                .write_bytes(ctx.mem_device(), 0, bytemuck::cast_slice(&index))
                .wrap_err("Failed to fill staging index buffer")?;
        }

        ctx.exec_commands(|ctx, cmd_buf| unsafe {
            ctx.device().cmd_copy_buffer(
                cmd_buf,
                staging_vertex_buffer.object(),
                target_vertex_buffer.object(),
                &[vk::BufferCopyBuilder::new()
                    .src_offset(0)
                    .dst_offset(0)
                    .size(vertex_buffer_size as _)],
            );

            ctx.device().cmd_copy_buffer(
                cmd_buf,
                staging_index_buffer.object(),
                target_index_buffer.object(),
                &[vk::BufferCopyBuilder::new()
                    .src_offset(0)
                    .dst_offset(0)
                    .size(index_buffer_size as _)],
            );
        })?;

        ctx.dealloc_buffer(staging_vertex_buffer);
        ctx.dealloc_buffer(staging_index_buffer);

        Ok(AllocatedModel {
            vertex_buffer: target_vertex_buffer,
            index_buffer: target_index_buffer,
            __index_vertex_types: Default::default(),
            vertex_count: vertices
                .len()
                .try_into()
                .expect("can't fit vertex count into usize"),
            index_count: index
                .len()
                .try_into()
                .expect("can't fit index count into usize"),
        })
    }

    pub unsafe fn deallocate(self, ctx: &VkContext) {
        ctx.dealloc_buffer(self.vertex_buffer);
        ctx.dealloc_buffer(self.index_buffer);
    }
}

mod main_pass;

use crate::Game;

use super::{
    swapchain::{Swapchain, SwapchainImage},
    vk_context::VkContext,
    PerInFlight, FRAMES_IN_FLIGHT,
};
use color_eyre::eyre::{Result, WrapErr};
use erupt::vk;
use glam::Mat4;
use main_pass::MainPass;

pub struct Passes {
    command_pool: vk::CommandPool,
    command_buffers: PerInFlight<vk::CommandBuffer>,
    main_pass: MainPass,
}

impl Passes {
    pub fn new(ctx: &VkContext, swapchain: &Swapchain) -> Result<Passes> {
        let command_pool_info = vk::CommandPoolCreateInfoBuilder::new()
            .flags(vk::CommandPoolCreateFlags::RESET_COMMAND_BUFFER)
            .queue_family_index(ctx.physical().queue_family());

        let command_pool = unsafe {
            ctx.device()
                .create_command_pool(&command_pool_info, None)
                .result()
                .wrap_err("Failed to create passes command pool")?
        };

        let command_buffers = ctx.allocate_command_buffers(
            command_pool,
            vk::CommandBufferLevel::PRIMARY,
            FRAMES_IN_FLIGHT,
        )?;

        Ok(Passes {
            command_pool,
            command_buffers: command_buffers.into(),
            main_pass: MainPass::new(ctx, swapchain).wrap_err("Failed to create main pass")?,
        })
    }

    pub fn run(
        &mut self,
        game: &Game,
        ctx: &VkContext,
        in_flight_idx: usize,
        swapchain: &Swapchain,
        swapchain_image: &SwapchainImage,
        swapchain_recreated: bool,
        objects: &[(&str, Mat4)],
    ) -> Result<vk::CommandBuffer> {
        let command_buffer = self.command_buffers.0[in_flight_idx];

        let begin_info = vk::CommandBufferBeginInfoBuilder::new();
        unsafe {
            ctx.device()
                .begin_command_buffer(command_buffer, &begin_info)
                .result()
                .wrap_err("Failed to begin command buffer")?;
        }

        self.main_pass
            .run(
                game,
                ctx,
                command_buffer,
                swapchain,
                swapchain_image,
                swapchain_recreated,
                objects,
            )
            .wrap_err("Failed to run main pass")?;

        unsafe {
            ctx.device()
                .end_command_buffer(command_buffer)
                .result()
                .wrap_err("Failed to end command buffer")?;
        }

        Ok(command_buffer)
    }

    pub fn destroy(&mut self, ctx: &VkContext) {
        self.main_pass.destroy(ctx);

        unsafe {
            ctx.device()
                .destroy_command_pool(Some(self.command_pool), None);
        }
    }
}

use color_eyre::eyre::{bail, eyre, Result, WrapErr};
use erupt::{vk, InstanceLoader};
use std::{borrow::Cow, ffi::CStr, os::raw::c_char};

macro_rules! feature {
    ($enabled:ident in $available:ident -> $name:ident) => {
        if $available.$name == vk::TRUE {
            $enabled.$name = vk::TRUE;
        } else {
            bail!("Unsupported feature: {}", stringify!($name));
        }
    };
}

pub struct Physical {
    handle: vk::PhysicalDevice,
    properties: vk::PhysicalDeviceProperties,
    enabled_features: vk::PhysicalDeviceFeatures,
    surface_formats: Vec<vk::SurfaceFormatKHR>,
    present_modes: Vec<vk::PresentModeKHR>,
    queue_family: u32,
}

impl Physical {
    pub fn new(
        instance: &InstanceLoader,
        handle: vk::PhysicalDevice,
        surface: vk::SurfaceKHR,
        device_extensions: &[*const c_char],
    ) -> Result<Physical> {
        let queue_families =
            unsafe { instance.get_physical_device_queue_family_properties(handle, None) }
                .into_iter()
                .enumerate();

        let mut queue_family = None;
        for (i, queue_family_properties) in queue_families {
            let i = i as u32;

            if queue_family_properties
                .queue_flags
                .contains(vk::QueueFlags::GRAPHICS)
                && unsafe {
                    instance
                        .get_physical_device_surface_support_khr(handle, i, surface)
                        .result()
                        .wrap_err("Failed to get surface support")?
                }
            {
                queue_family = Some(i);
                break;
            }
        }

        let queue_family =
            queue_family.ok_or_else(|| eyre!("Can't find appropriate queue family"))?;

        let extension_properties = unsafe {
            instance
                .enumerate_device_extension_properties(handle, None, None)
                .result()
                .wrap_err("Failed to enumerate device extensions")?
        };

        let unsupported_extensions: Vec<_> = unsafe {
            device_extensions
                .iter()
                .map(|&extension| CStr::from_ptr(extension))
                .filter(|&extension| {
                    !extension_properties.iter().any(|properties| {
                        CStr::from_ptr(properties.extension_name.as_ptr()) == extension
                    })
                })
                .collect()
        };

        if !unsupported_extensions.is_empty() {
            bail!(
                "Device is missing required extensions: {:?}",
                unsupported_extensions
            );
        }

        let surface_formats = unsafe {
            instance
                .get_physical_device_surface_formats_khr(handle, surface, None)
                .result()
                .wrap_err("Failed to get surface formats")?
        };

        let present_modes = unsafe {
            instance
                .get_physical_device_surface_present_modes_khr(handle, surface, None)
                .result()
                .wrap_err("Failed to get present modes")?
        };

        let available_features = unsafe { instance.get_physical_device_features(handle) };
        let mut enabled_features = vk::PhysicalDeviceFeatures::default();
        feature!(enabled_features in available_features -> fill_mode_non_solid);
        feature!(enabled_features in available_features -> fragment_stores_and_atomics);
        feature!(enabled_features in available_features -> vertex_pipeline_stores_and_atomics);

        let properties = unsafe { instance.get_physical_device_properties(handle) };
        Ok(Physical {
            handle,
            properties,
            enabled_features,
            surface_formats,
            present_modes,
            queue_family,
        })
    }

    pub fn handle(&self) -> vk::PhysicalDevice {
        self.handle
    }

    pub fn score(&self) -> usize {
        match self.properties.device_type {
            vk::PhysicalDeviceType::DISCRETE_GPU => 2,
            vk::PhysicalDeviceType::INTEGRATED_GPU => 1,
            _ => 0,
        }
    }

    pub fn name(&self) -> Cow<str> {
        unsafe { CStr::from_ptr(self.properties.device_name.as_ptr()) }.to_string_lossy()
    }

    pub fn enabled_features(&self) -> &vk::PhysicalDeviceFeatures {
        &self.enabled_features
    }

    pub fn surface_formats(&self) -> &[vk::SurfaceFormatKHR] {
        &self.surface_formats
    }

    pub fn present_modes(&self) -> &[vk::PresentModeKHR] {
        &self.present_modes
    }

    pub fn queue_family(&self) -> u32 {
        self.queue_family
    }
}

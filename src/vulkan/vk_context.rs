use super::physical::Physical;
use crate::window::Window;
use color_eyre::{
    eyre::{eyre, Result, WrapErr},
    Report,
};
use erupt::{
    cstr, utils::surface, vk, DefaultEntryLoader, DeviceLoader, EntryLoader, ExtendableFromConst,
    InstanceLoader,
};
use gpu_alloc::{GpuAllocator, MemoryBlock};
use gpu_alloc_erupt::EruptMemoryDevice;
use parking_lot::Mutex;
use std::{
    ffi::{c_void, CStr, CString},
    os::raw::c_char,
};

const LAYER_KHRONOS_VALIDATION: *const c_char = cstr!("VK_LAYER_KHRONOS_validation");
const VALIDATION_BLACKLIST: &[&str] = &[
    "Device Extension: ",
    "Loading layer library",
    "Unloading layer library",
    "Inserted device layer",
];

unsafe extern "system" fn vulkan_validation(
    _message_severity: vk::DebugUtilsMessageSeverityFlagBitsEXT,
    _message_types: vk::DebugUtilsMessageTypeFlagsEXT,
    p_callback_data: *const vk::DebugUtilsMessengerCallbackDataEXT,
    _p_user_data: *mut c_void,
) -> vk::Bool32 {
    let message = CStr::from_ptr((*p_callback_data).p_message).to_string_lossy();
    if !VALIDATION_BLACKLIST
        .iter()
        .any(|entry| message.contains(entry))
    {
        log::warn!("Vulkan validation: {}", message);
    }

    vk::FALSE
}

pub struct VkContext {
    device: DeviceLoader,
    physical: Physical,
    instance: InstanceLoader,
    _entry: DefaultEntryLoader,
    messenger: Option<vk::DebugUtilsMessengerEXT>,
    surface: vk::SurfaceKHR,
    queue: vk::Queue,
    allocator: Mutex<GpuAllocator<vk::DeviceMemory>>,
    transient_command_pool: vk::CommandPool,
}

impl VkContext {
    pub fn new(window: &Window, validation: bool) -> Result<VkContext> {
        let entry = EntryLoader::new().wrap_err("Failed to initialize entry loader")?;

        let name = CString::new("frizgame1").unwrap();
        let app_info = vk::ApplicationInfoBuilder::new()
            .application_name(&name)
            .engine_name(&name)
            .api_version(vk::make_version(1, 1, 0));

        let mut layers = vec![];
        let mut instance_extensions = surface::enumerate_required_extensions(&window.context())
            .result()
            .wrap_err("Failed to enumerate required surface instance extensions")?;
        if validation {
            layers.push(LAYER_KHRONOS_VALIDATION);
            instance_extensions.push(vk::EXT_DEBUG_UTILS_EXTENSION_NAME);
        }

        let instance_info = vk::InstanceCreateInfoBuilder::new()
            .application_info(&app_info)
            .enabled_layer_names(&layers)
            .enabled_extension_names(&instance_extensions);
        let instance = unsafe { InstanceLoader::new(&entry, &instance_info, None) }
            .wrap_err("Failed to initialize instance loader")?;

        let messenger = if validation {
            let messenger_info = vk::DebugUtilsMessengerCreateInfoEXTBuilder::new()
                .message_severity(
                    vk::DebugUtilsMessageSeverityFlagsEXT::INFO_EXT
                        | vk::DebugUtilsMessageSeverityFlagsEXT::VERBOSE_EXT
                        | vk::DebugUtilsMessageSeverityFlagsEXT::WARNING_EXT
                        | vk::DebugUtilsMessageSeverityFlagsEXT::ERROR_EXT,
                )
                .message_type(
                    vk::DebugUtilsMessageTypeFlagsEXT::GENERAL_EXT
                        | vk::DebugUtilsMessageTypeFlagsEXT::VALIDATION_EXT
                        | vk::DebugUtilsMessageTypeFlagsEXT::PERFORMANCE_EXT,
                )
                .pfn_user_callback(Some(vulkan_validation));

            let messenger =
                unsafe { instance.create_debug_utils_messenger_ext(&messenger_info, None) }
                    .result()
                    .wrap_err("Failed to create debug messenger")?;

            Some(messenger)
        } else {
            None
        };

        let surface = unsafe { surface::create_surface(&instance, &window.context(), None) }
            .result()
            .wrap_err("Failed to create surface")?;

        let mut device_extensions = vec![vk::KHR_SWAPCHAIN_EXTENSION_NAME];
        if validation {
            device_extensions.push(vk::KHR_SHADER_NON_SEMANTIC_INFO_EXTENSION_NAME);
        }

        let physicals: Result<Vec<_>> = unsafe { instance.enumerate_physical_devices(None) }
            .result()
            .map_err(Report::new)
            .and_then(|handles| {
                handles
                    .into_iter()
                    .map(|handle| Physical::new(&instance, handle, surface, &device_extensions))
                    .collect()
            });

        let physical = physicals
            .wrap_err("Failed to enumerate physicals")?
            .into_iter()
            .max_by_key(|physical| physical.score())
            .ok_or_else(|| eyre!("Can't find any fitting physical devices"))?;
        log::info!("Using device: {}", physical.name());

        let queue_priorities = &[1.0];
        let queue_infos = vec![vk::DeviceQueueCreateInfoBuilder::new()
            .queue_priorities(queue_priorities)
            .queue_family_index(physical.queue_family())];

        let device_info = vk::DeviceCreateInfoBuilder::new()
            .queue_create_infos(&queue_infos)
            .enabled_layer_names(&layers)
            .enabled_extension_names(&device_extensions)
            .enabled_features(physical.enabled_features());
        let device = unsafe { DeviceLoader::new(&instance, physical.handle(), &device_info, None) }
            .wrap_err("Failed to initialize device loader")?;
        let queue = unsafe { device.get_device_queue(physical.queue_family(), 0) };

        // https://github.com/gfx-rs/wgpu/blob/2287ae3f8a7246e8b47b73a1de6fe2410c42b8c8/wgpu-core/src/device/alloc.rs#L23-L30
        let allocator_config = gpu_alloc::Config {
            dedicated_threshold: 32 << 20,
            preferred_dedicated_threshold: 8 << 20,
            transient_dedicated_threshold: 128 << 20,
            linear_chunk: 128 << 20,
            minimal_buddy_size: 1 << 10,
            initial_buddy_dedicated_size: 8 << 20,
        };

        let allocator_properties =
            unsafe { gpu_alloc_erupt::device_properties(&instance, physical.handle()) }
                .wrap_err("Failed to get device properties for allocator")?;
        let allocator = GpuAllocator::new(allocator_config, allocator_properties);

        let transient_command_pool_info = vk::CommandPoolCreateInfoBuilder::new()
            .flags(vk::CommandPoolCreateFlags::TRANSIENT)
            .queue_family_index(physical.queue_family());

        let transient_command_pool = unsafe {
            device
                .create_command_pool(&transient_command_pool_info, None)
                .result()
                .wrap_err("Failed to create transient command pool")?
        };

        Ok(VkContext {
            device,
            physical,
            instance,
            _entry: entry,
            messenger,
            surface,
            queue,
            allocator: Mutex::new(allocator),
            transient_command_pool,
        })
    }

    pub fn name_object(&self, ty: vk::ObjectType, handle: u64, name: &str) -> Result<()> {
        if self.messenger.is_some() {
            let name = CString::new(name).expect("Failed to create C String");
            let name_info = vk::DebugUtilsObjectNameInfoEXTBuilder::new()
                .object_type(ty)
                .object_handle(handle)
                .object_name(&name);

            unsafe {
                self.device()
                    .set_debug_utils_object_name_ext(&name_info)
                    .result()
                    .wrap_err("Failed to name object")
            }
        } else {
            Ok(())
        }
    }

    pub fn create_semaphore(&self, name: &str, timeline: bool) -> Result<vk::Semaphore> {
        let semaphore_type_info = vk::SemaphoreTypeCreateInfoKHRBuilder::new()
            .semaphore_type(if timeline {
                vk::SemaphoreTypeKHR::TIMELINE_KHR
            } else {
                vk::SemaphoreTypeKHR::BINARY_KHR
            })
            .initial_value(0);

        let semaphore_info =
            vk::SemaphoreCreateInfoBuilder::new().extend_from(&semaphore_type_info);

        unsafe {
            let semaphore = self
                .device()
                .create_semaphore(&semaphore_info, None)
                .result()
                .wrap_err("Failed to create semaphore")?;

            self.name_object(vk::Semaphore::TYPE, semaphore.0, name)?;
            Ok(semaphore)
        }
    }

    pub fn create_fence(&self, name: &str, signaled: bool) -> Result<vk::Fence> {
        let mut flags = vk::FenceCreateFlags::empty();
        if signaled {
            flags |= vk::FenceCreateFlags::SIGNALED;
        }

        let fence_info = vk::FenceCreateInfoBuilder::new().flags(flags);

        unsafe {
            let fence = self
                .device()
                .create_fence(&fence_info, None)
                .result()
                .wrap_err("Failed to create fence")?;

            self.name_object(vk::Fence::TYPE, fence.0, name)?;
            Ok(fence)
        }
    }

    pub fn allocate_command_buffers(
        &self,
        pool: vk::CommandPool,
        level: vk::CommandBufferLevel,
        amount: usize,
    ) -> Result<Vec<vk::CommandBuffer>> {
        let command_buffers_info = vk::CommandBufferAllocateInfoBuilder::new()
            .command_pool(pool)
            .level(level)
            .command_buffer_count(amount as _);

        unsafe {
            self.device()
                .allocate_command_buffers(&command_buffers_info)
                .result()
                .wrap_err("Failed to allocate command buffers")
        }
    }

    pub fn create_shader_module(&self, code: &[u8]) -> Result<vk::ShaderModule> {
        let decoded = erupt::utils::decode_spv(code).wrap_err("Failed to decode SPIR-V")?;
        let module_info = vk::ShaderModuleCreateInfoBuilder::new().code(&decoded);

        unsafe {
            self.device()
                .create_shader_module(&module_info, None)
                .result()
                .wrap_err("Failed to create shader module")
        }
    }

    pub fn create_pipeline_layout(
        &self,
        set_layouts: &[vk::DescriptorSetLayout],
        push_constant_ranges: &[vk::PushConstantRangeBuilder],
    ) -> Result<vk::PipelineLayout> {
        let pipeline_layout_info = vk::PipelineLayoutCreateInfoBuilder::new()
            .set_layouts(set_layouts)
            .push_constant_ranges(push_constant_ranges);

        unsafe {
            self.device()
                .create_pipeline_layout(&pipeline_layout_info, None)
                .result()
                .wrap_err("Failed to create pipeline layout")
        }
    }

    pub fn create_graphics_pipeline(
        &self,
        pipeline_info: vk::GraphicsPipelineCreateInfoBuilder,
        name: &str,
    ) -> Result<vk::Pipeline> {
        let pipeline_info = &[pipeline_info];
        unsafe {
            let pipelines = self
                .device()
                .create_graphics_pipelines(None, pipeline_info, None)
                .result()
                .wrap_err("Failed to create graphics pipeline")?;
            let pipeline = pipelines[0];
            self.name_object(vk::Pipeline::TYPE, pipeline.0, name)?;
            Ok(pipeline)
        }
    }

    pub fn create_image_view(
        &self,
        image: vk::Image,
        format: vk::Format,
        aspect_mask: vk::ImageAspectFlags,
    ) -> Result<vk::ImageView> {
        let image_view_info = vk::ImageViewCreateInfoBuilder::new()
            .image(image)
            .view_type(vk::ImageViewType::_2D)
            .format(format)
            .subresource_range(vk::ImageSubresourceRange {
                aspect_mask,
                base_mip_level: 0,
                level_count: 1,
                base_array_layer: 0,
                layer_count: 1,
            });

        unsafe {
            self.device()
                .create_image_view(&image_view_info, None)
                .result()
                .wrap_err("Failed to create image view")
        }
    }

    pub fn alloc_buffer(
        &self,
        size: usize,
        buf_usage: vk::BufferUsageFlags,
        mem_usage: gpu_alloc::UsageFlags,
    ) -> Result<Allocation<vk::Buffer>> {
        let buffer_info = vk::BufferCreateInfoBuilder::new()
            .size(size as _)
            .usage(buf_usage)
            .sharing_mode(vk::SharingMode::EXCLUSIVE);

        unsafe {
            let buffer = self
                .device()
                .create_buffer(&buffer_info, None)
                .result()
                .wrap_err("Failed to create buffer object")?;

            let mem_requirements = self.device().get_buffer_memory_requirements(buffer);
            let alloc_request = gpu_alloc::Request {
                size: mem_requirements.size,
                align_mask: mem_requirements.alignment,
                usage: mem_usage,
                memory_types: mem_requirements.memory_type_bits,
            };

            let block = self
                .allocator
                .lock()
                .alloc(self.mem_device(), alloc_request)
                .wrap_err("Failed to allocate memory for buffer")?;

            self.device()
                .bind_buffer_memory(buffer, *block.memory(), block.offset())
                .result()
                .wrap_err("Failed to bind buffer memory")?;

            Ok(Allocation {
                object: buffer,
                block,
            })
        }
    }

    pub fn dealloc_buffer(
        &self,
        Allocation {
            object: buffer,
            block,
        }: Allocation<vk::Buffer>,
    ) {
        unsafe {
            self.allocator.lock().dealloc(self.mem_device(), block);
            self.device().destroy_buffer(Some(buffer), None);
        }
    }

    pub fn alloc_image(
        &self,
        extent: vk::Extent2D,
        format: vk::Format,
        tiling: vk::ImageTiling,
        img_usage: vk::ImageUsageFlags,
        mem_usage: gpu_alloc::UsageFlags,
    ) -> Result<Allocation<vk::Image>> {
        let image_info = vk::ImageCreateInfoBuilder::new()
            .image_type(vk::ImageType::_2D)
            .extent(vk::Extent3D {
                width: extent.width,
                height: extent.height,
                depth: 1,
            })
            .mip_levels(1)
            .array_layers(1)
            .format(format)
            .tiling(tiling)
            .initial_layout(vk::ImageLayout::UNDEFINED)
            .usage(img_usage)
            .samples(vk::SampleCountFlagBits::_1)
            .sharing_mode(vk::SharingMode::EXCLUSIVE);

        unsafe {
            let image = self
                .device()
                .create_image(&image_info, None)
                .result()
                .context("Failed to create image object")?;

            let mem_requirements = self.device().get_image_memory_requirements(image);
            let alloc_request = gpu_alloc::Request {
                size: mem_requirements.size,
                align_mask: mem_requirements.alignment,
                usage: mem_usage,
                memory_types: mem_requirements.memory_type_bits,
            };

            let block = self
                .allocator
                .lock()
                .alloc(self.mem_device(), alloc_request)
                .wrap_err("Failed to allocate memory for image")?;

            self.device()
                .bind_image_memory(image, *block.memory(), block.offset())
                .result()
                .wrap_err("Failed to bind image memory")?;

            Ok(Allocation {
                object: image,
                block,
            })
        }
    }

    pub fn dealloc_image(
        &self,
        Allocation {
            object: image,
            block,
        }: Allocation<vk::Image>,
    ) {
        unsafe {
            self.allocator.lock().dealloc(self.mem_device(), block);
            self.device().destroy_image(Some(image), None);
        }
    }

    pub fn exec_commands(&self, f: impl Fn(&VkContext, vk::CommandBuffer)) -> Result<()> {
        let fence = self.create_fence("Transient Fence", false)?;
        let cmd_buf = self.allocate_command_buffers(
            self.transient_command_pool,
            vk::CommandBufferLevel::PRIMARY,
            1,
        )?[0];

        let begin_info = vk::CommandBufferBeginInfoBuilder::new()
            .flags(vk::CommandBufferUsageFlags::ONE_TIME_SUBMIT);

        unsafe {
            self.device()
                .begin_command_buffer(cmd_buf, &begin_info)
                .result()
                .wrap_err("Failed to begin transient command buffer")?;

            f(self, cmd_buf);

            self.device()
                .end_command_buffer(cmd_buf)
                .result()
                .wrap_err("Failed to end transient command buffer")?;

            self.device()
                .queue_submit(
                    self.queue(),
                    &[vk::SubmitInfoBuilder::new().command_buffers(&[cmd_buf])],
                    Some(fence),
                )
                .result()
                .wrap_err("Failed to submit transient commands")?;

            self.device()
                .wait_for_fences(&[fence], true, u64::MAX)
                .result()
                .wrap_err("Failed to wait for transient fence")?;

            self.device()
                .free_command_buffers(self.transient_command_pool, &[cmd_buf]);
            self.device().destroy_fence(Some(fence), None);
        }

        Ok(())
    }

    pub fn instance(&self) -> &InstanceLoader {
        &self.instance
    }

    pub fn device(&self) -> &DeviceLoader {
        &self.device
    }

    pub fn physical(&self) -> &Physical {
        &self.physical
    }

    pub fn surface(&self) -> vk::SurfaceKHR {
        self.surface
    }

    pub fn queue(&self) -> vk::Queue {
        self.queue
    }

    pub fn mem_device(&self) -> &EruptMemoryDevice {
        EruptMemoryDevice::wrap(self.device())
    }

    pub fn destroy(&self) -> Result<()> {
        unsafe {
            self.device()
                .destroy_command_pool(Some(self.transient_command_pool), None);

            self.allocator
                .lock()
                .cleanup(EruptMemoryDevice::wrap(&self.device));

            self.device.destroy_device(None);

            self.instance.destroy_surface_khr(Some(self.surface), None);

            if let Some(messenger) = self.messenger {
                self.instance
                    .destroy_debug_utils_messenger_ext(Some(messenger), None);
            }

            self.instance.destroy_instance(None);
        }

        Ok(())
    }
}

pub struct Allocation<O> {
    object: O,
    block: MemoryBlock<vk::DeviceMemory>,
}

impl<O: Clone> Allocation<O> {
    pub fn object(&self) -> O {
        self.object.clone()
    }

    pub fn block(&mut self) -> &mut MemoryBlock<vk::DeviceMemory> {
        &mut self.block
    }
}

mod config;
mod logic;
mod vulkan;
mod window;

use color_eyre::eyre::{Result, WrapErr};
use config::{Config, Framerate};
use env::VarError;
use glam::DVec2;
use glfw::{Action, Key, WindowEvent};
use hecs::World;
use log::info;
use logic::Logic;
use spin_sleep::LoopHelper;
use std::{env, time::Instant};
use structopt::StructOpt;
use vulkan::Vulkan;
use window::Window;

#[macro_export]
macro_rules! include_res {
    ($file:expr $(,)?) => {
        include_bytes!(concat!(env!("CARGO_MANIFEST_DIR"), "/res/", $file))
    };
}

pub struct Game {
    pub world: World,
    pub opt: Opt,
    pub config: Config,
    pub start: Instant,
    pub events: Vec<WindowEvent>,
    pub grabbed: bool,
    pub running: bool,
    pub delta_time: f64,
    pub physics_time_scale: f64,
    pub cursor_delta: DVec2,
    pub logic: Logic,
}

impl Game {
    pub fn new(opt: Opt, config: Config) -> Game {
        Game {
            world: World::new(),
            opt,
            config,
            start: Instant::now(),
            events: Vec::new(),
            grabbed: false,
            running: true,
            physics_time_scale: 1.0,
            //physics_time_scale: 0.06,
            delta_time: 0.0,
            cursor_delta: DVec2::ZERO,
            logic: Logic::new(),
        }
    }

    pub fn process(&mut self) {
        for event in self.events.iter() {
            match event {
                WindowEvent::Close => self.running = false,
                WindowEvent::Key(key, _scancode, action, _modifiers) => match (key, action) {
                    (Key::Escape, Action::Press) => self.running = false,
                    (Key::F, Action::Press) => self.config.fullscreen ^= true,
                    (Key::G, Action::Press) => self.grabbed ^= true,
                    _ => (),
                },
                _ => (),
            }
        }

        Logic::process(self);
    }
}

#[derive(Debug, StructOpt)]
pub struct Opt {
    /// Enable Vulkan validation
    #[structopt(short, long)]
    validation: bool,
}

fn main() -> Result<()> {
    env_set_not_set("RUST_BACKTRACE", "1");
    env_set_not_set("RUST_LIB_BACKTRACE", "1");
    env_set_not_set("RUST_LOG", "frizgame1=info");
    color_eyre::install()?;
    pretty_env_logger::init();

    let opt = Opt::from_args();
    log::debug!("Command line options:\n{:#?}", opt);

    let config = Config::load().wrap_err("Failed to load config")?;
    log::debug!("Config:\n{:#?}", config);

    let loop_helper_builder = LoopHelper::builder();
    let mut loop_helper = match &config.framerate {
        Framerate::Unlimited | Framerate::Sync => loop_helper_builder.build_without_target_rate(),
        Framerate::Limited(fps) => loop_helper_builder.build_with_target_rate(*fps),
    };

    let mut game = Game::new(opt, config);
    let mut window = Window::new(&game).wrap_err("Failed to create Window")?;
    let mut vulkan = Vulkan::new(&window, &game).wrap_err("Failed to initialize renderer")?;

    log::info!("Initialization complete");
    for frame in 0u64.. {
        game.delta_time = loop_helper.loop_start_s();
        log::trace!("New frame ({})", frame);
        if let Some(fps) = loop_helper.report_rate() {
            log::info!("FPS: {:.2}", fps);
        }

        log::trace!("Fetching events");
        game.cursor_delta = window.fetch_events(&mut game.events).into();

        log::trace!("Running game processing");
        game.process();

        if !game.running {
            break;
        }

        window.set_grabbed(game.grabbed);
        window.set_fullscreen(game.config.fullscreen);

        log::trace!("Processing Vulkan frame");
        vulkan.frame(&game).wrap_err("Failed to process frame")?;

        loop_helper.loop_sleep();
    }

    vulkan.destroy().wrap_err("Failed to destroy Vulkan")?;

    game.config.save()?;
    info!("Exiting main function");
    Ok(())
}

pub fn env_set_not_set(key: &str, value: &str) {
    if let Err(VarError::NotPresent) = env::var(key) {
        env::set_var(key, value);
    }
}
